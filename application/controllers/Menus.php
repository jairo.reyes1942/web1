<?php
class Menus extends CI_Controller
{
  // constructor
    function __construct()
    {
      parent::__construct();
    }
    // Renderizacuion de la vistaqu emuestra los desayunos
    public function mision(){
      $this->load->view('header');
      $this->load->view('menus/mision');
      $this->load->view('footer');
    }
    public function vision(){
      $this->load->view('header');
      $this->load->view('menus/vision');
      $this->load->view('footer');
    }
    public function ubicacion(){
      $this->load->view('header');
      $this->load->view('menus/ubicacion');
      $this->load->view('footer');
    }
    public function desayunos(){
      $this->load->view('header');
      $this->load->view('menus/desayunos');
      $this->load->view('footer');
    }
    public function almuerzos(){
      $this->load->view('header');
      $this->load->view('menus/almuerzos');
      $this->load->view('footer');
    }
    public function meriendas(){
      $this->load->view('header');
      $this->load->view('menus/meriendas');
      $this->load->view('footer');
    }
    public function cartas(){
      $this->load->view('header');
      $this->load->view('menus/cartas');
      $this->load->view('footer');
    }
}//no borrar


 ?>
