<center><h2><b>UBICACIÓN EN EL MAPA</b> </h2></center>
  <div class="row">
    <div class="col-md-12 text-center">
      <div class="col-md-2 text-center">
      </div>
      <div class="col-md-8 text-center">
       <div id="mapaDireccion" style="width:100%;height:500px;"></div>

        <script type="text/javascript">
          function initMap(){
            // creando el punto central del mapa
            var coordenadaCentral=new google.maps.LatLng(-0.9177157047940647, -78.63294289005339);
            // creando el mapa
            var mapa1=new google.maps.Map(document.getElementById('mapaDireccion'),{
              center:coordenadaCentral,zoom:18,mapTypeId:google.maps.MapTypeId.ROADMAP
            });
          }
        </script>
      </div>
      <div class="col-md-2 text-center">
      </div>
   </div>
  </div>
