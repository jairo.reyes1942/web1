<div class="row">
  <div class="col-md-3 text-center">

  </div>
  <div class="col-md-6 text-center">
    <h1><b>Visión</b> </h1>
    <table class="table table-bordered" >
    <html>
    <head>
      <style>
        .justified {
          text-align: justify;
        }
      </style>
    </head>
    <body>
      <h3><p class="justified">Nuestra visión es convertirnos en el restaurante líder en nuestra área, reconocido tanto a nivel local como nacional por nuestra cocina excepcional, servicio impecable y ambiente único. Nos esforzamos por ser un destino gastronómico de renombre, donde los comensales pueden deleitarse con una variedad de sabores y experiencias culinarias. Buscamos establecer alianzas con proveedores locales para garantizar la frescura y calidad de nuestros ingredientes, y colaborar con chefs talentosos y apasionados que compartan nuestra visión. Nuestro objetivo es ser el lugar preferido tanto para ocasiones especiales como para comidas cotidianas, y ser reconocidos como un referente de excelencia en la industria de la restauración.</p></h3>
    </body>
    </html>
    </table>
  </div>
  <div class="col-md-3 text-center">

  </div>


</div>
