<div class="row">
  <div class="col-md-3 text-center">

  </div>
  <div class="col-md-6 text-center">
    <h1><b>Misión</b> </h1>
    <table class="table table-bordered" >
    <html>
    <head>
      <style>
        .justified {
          text-align: justify;
        }
      </style>
    </head>
    <body>
      <h3><p class="justified">Nuestra misión es brindar una experiencia culinaria excepcional a nuestros comensales. Nos esforzamos por ofrecer platos exquisitos preparados con ingredientes frescos y de calidad, presentados con creatividad y atención al detalle. Buscamos superar las expectativas de nuestros clientes al proporcionar un servicio amable y profesional en un ambiente acogedor y agradable. Nuestro objetivo es convertirnos en el restaurante de referencia para los amantes de la buena comida y ser reconocidos por nuestra excelencia gastronómica y nuestra pasión por la satisfacción del cliente.</p></h3>
    </body>
    </html>
    </table>
  </div>
  <div class="col-md-3 text-center">

  </div>

</div>
